package progr_sieciowe2;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Main {

    public static void main(String[] args) throws UnknownHostException, IOException {

        InetAddress cel = InetAddress.getByName("www.trojmiasto.pl");
        Socket s = null;
        s = new Socket(cel, 80);
        InputStream iStream = s.getInputStream();
        OutputStream oStream = s.getOutputStream();
        
        BufferedReader wejscie = new BufferedReader(new InputStreamReader(iStream));
        PrintWriter wyjscie = new PrintWriter(new OutputStreamWriter(oStream));
                
        String naglowek = "GET / HTTP/1.1\n\n";
        wyjscie.write(naglowek);
        wyjscie.flush();
        
        String bufor="";
        while ((bufor = wejscie.readLine()) !=null) {
            System.out.println(wejscie.readLine());
        }
        
        s.close();
    
        
    }

}
