
package Strumienie_w_sieci;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Main {

    public static void main(String[] args) throws IOException {
        
        try {
            InetAddress cel = InetAddress.getByName("whois.internic.net");
            Socket s=null;
            try {
                s = new Socket(cel, 43);
                InputStream iStream = s.getInputStream();
                
                int bufor;

                OutputStream oStream = s.getOutputStream();
                oStream.write(1);
                
                while((bufor = iStream.read()) !=-1){
                    System.out.println((char)bufor);
                }
                
                
            } catch (IOException ex) {
                System.out.println(ex);
            }finally{
                s.close();
                
            }
        } catch (UnknownHostException ex) {
            System.out.println(ex);
        }
    }
    
}
