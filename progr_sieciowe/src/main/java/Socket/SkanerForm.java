package Socket;

import javax.swing.*;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

public class SkanerForm extends JFrame implements ActionListener {

    JTextField adres, portOd, portDo;
    JTextArea wyniki;

    public SkanerForm(int x, int y) throws IOException {
        setLayout(new FlowLayout());
        setSize(x, y);
        setTitle("Skaner portów");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);

        add(new JLabel("Port pocz."));
        portOd = new JTextField(20);
        portOd.setText("75");
        add(portOd);

        add(new JLabel("Port końcowy."));
        portDo = new JTextField(20);
        portDo.setText("80");
        add(portDo);

        add(new JLabel("Adres"));
        adres = new JTextField(20);

        adres.setText(InetAddress.getByName("localhost").getHostAddress());
        add(adres);

        JButton wyczysc = new JButton("Wyczyść");
        wyczysc.addActionListener(this);
        add(wyczysc);

        JButton skanuj = new JButton("Skanuj");
        skanuj.addActionListener(this);
        add(skanuj);

        wyniki = new JTextArea(15, 20);

        JScrollPane scroll = new JScrollPane(wyniki, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        add(scroll);

        setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Wyczyść")) {
            portOd.setText("");
            portDo.setText("");
            adres.setText("");
            wyniki.setText("");
        }
        if (e.getActionCommand().equals("Skanuj")) {
            String answer = chk(portOd.getText(), portDo.getText());
            if (answer != "") {
                wyniki.setText(answer);
            } else {
                answer = getResult(portOd.getText(), portDo.getText(), adres.getText());
                wyniki.setText(answer);
            }
        }
    }

    public String chk(String from, String to) {
        String answer = "";

        try {
            Integer.parseInt(from);
        } catch (NumberFormatException numberFormatException) {
            return answer = "W polu 'Port pocz.' nie podano liczby";
        }
        try {
            Integer.parseInt(to);
        } catch (NumberFormatException numberFormatException) {
            return answer = "W polu 'Port końcowy.' nie podano liczby";
        }

        if (Integer.parseInt(from) > Integer.parseInt(to)) {
            return answer = "Port końcowy jest mniejszy niż początkowy";
        }

        return answer;
    }

    public String getResult(String from, String to, String adres) {
        String wynik="";
        for (int i = Integer.parseInt(from); i <= Integer.parseInt(to); i++) {

            Socket so = new Socket();
            try {
                so.connect(new InetSocketAddress(adres, i), 20);
                System.out.println("Port " + i + " jest otwarty \n");
                wynik = wynik + "Port " + i + " jest otwarty \n";
            } catch (IOException ex) {
                System.out.println(ex);
                wynik = wynik + "Port " + i + " jest zamknięty \n";
            }

            try {
                so.close();
            } catch (IOException ex) {
                System.out.println(ex);
            }
        }

        return wynik;
    }

}
