package pl.com.tok.progr_sieciowe;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Main {
    
    public static void main(String[] args) throws UnknownHostException, IOException {
        
        InetAddress test = InetAddress.getByName("www.microsoft.com"); //pierwszy nr domeny
        InetAddress[] test1 = InetAddress.getAllByName("microsoft.com"); //tablica z numerami domeny
    
        System.out.println(test+"\n --------------------");
        
        for (int i=0; i<test1.length;i++) {
            System.out.println(test1[i]);
            System.out.println(test1[i].getHostAddress());
        }
    
        System.out.println("--------------");
                    
        InetAddress test2 = InetAddress.getLocalHost(); //lokalny adres w sieci lan
        System.out.println(test2);
    
        InetAddress test3 = InetAddress.getLocalHost(); //lokalny adres w sieci lan
        System.out.println(test3);
        System.out.println(test3.getHostName());
        System.out.println(test3.getHostAddress());
         
        
        System.out.println("--------------");
    /*    
        Scanner scn = new Scanner(System.in);
        String address = scn.nextLine();
        InetAddress[] test4 = InetAddress.getAllByName(address);
        for (InetAddress inetAddress : test4) {
            System.out.println(inetAddress.getHostAddress());
        }
    */    
        System.out.println("-------------- ---");
 
        
        InetAddress test5 = InetAddress.getByName("www.wp.pl");
        System.out.println(test5);
        System.out.println(test5.getAddress()[0]);
        System.out.println(test5.getCanonicalHostName());
        System.out.println(test5.getHostAddress());
        System.out.println(test5.getHostName());
        System.out.println(test5.isReachable(14));
        
        System.out.println("- DANE Z URL -");
        
        URL test6 = new URL("https://szkoleniejava.slack.com/messages/CF0GSRTN0/");
        System.out.println(test6.getAuthority());
        System.out.println(test6.getDefaultPort());
        System.out.println(test6.getFile());
        System.out.println(test6.getHost());
        System.out.println(test6.getPath());
        System.out.println(test6.getProtocol());
        System.out.println(test6.getFile());
        
        System.out.println("- ---- -");
        
    }
}
